import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { ProductComponent } from '../components/product/product.component';

const routes:Routes=[{path:'', component:HomeComponent}, 
{path:'product', component:ProductComponent}];

@NgModule({
  // declarations: [HomeComponent, ProductComponent],//
  imports: [
    CommonModule, RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]

})
export class AppRoutingModule { }
// export const routingComponents=[HomeComponent, ProductComponent];