import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MatSliderModule } from '@angular/material/slider';
import { DataService } from './data.service';
import { ProductComponent } from './components/product/product.component';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule} from './app-routing/app-routing.module';
import { ProductCardComponent } from './components/product-card/product-card.component';
import {HttpClientModule} from '@angular/common/http';
import { ProductModalComponent } from './components/product-modal/product-modal.component';
import { LeftSliderComponent } from './components/left-slider/left-slider.component';
import { MatSelectModule} from '@angular/material/select';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductWorkingComponent } from './components/product-working/product-working.component';
import { MobileViewComponent } from './components/mobile-view/mobile-view.component';
import {MaterialModule} from './material.module';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ProductComponent,
    ProductCardComponent,
    ProductModalComponent,
    LeftSliderComponent,
    ProductWorkingComponent,
    MobileViewComponent,
    
  ],
  imports: [
    MatSliderModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatSelectModule,
    MatButtonToggleModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
