import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductDataService {
  overview_toggle:any=false;
  itemIndex:any;
  selectedProduct:any;
  dataArray:any;
  filteredDataArray:any;
  isPrevButtonDisabled:any=false;
  isNextButtonDisabled:any=false;

  constructor(private _http:HttpClient) { 

  }
  // private _url:string ="https://98b389d9-3f2c-431e-92a6-4fd9a942a8ef.mock.pstmn.io/quote";
  private _url:string="https://6051b8b8fb49dc00175b6997.mockapi.io/api/quotes";
  getProductData(){
    let bodyParam=null;
    // return this._http.get(this._url);
    return this._http.post(this._url, bodyParam);
  }
}
