import { Component, OnInit } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { ProductDataService } from 'src/app/product-data.service';

@Component({
  selector: 'app-left-slider',
  templateUrl: './left-slider.component.html',
  styleUrls: ['./left-slider.component.css']
})
export class LeftSliderComponent implements OnInit {
  heaterType='Natural Gas';
  emergencyButton=false;
  sliderVolume:any=50;
  peopleObject:any={30:"1-2", 40:"2-4", 50:"3-5", 60:"3-5", 70:"4-6", 80:"4-6"};
  peopleCount:any=this.peopleObject[this.sliderVolume];
  fuelType: string[] = [
    'Natural Gas', 'Oil Fuel', 'Propane', 'Electric', 'New Construction', 'Other'
  ];

  constructor(public productOverview: ProductDataService) { }

  ngOnInit(): void {
  }
  onInputChange(event: MatSliderChange) {
    this.sliderVolume=event.value;
    this.peopleCount=this.peopleObject[this.sliderVolume];
    this.productOverview.filteredDataArray = this.productOverview.dataArray.filter(
      (obj: any) => obj.volume === this.sliderVolume);
      console.log(this.productOverview.filteredDataArray);
  }
}
