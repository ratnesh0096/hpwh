import { Component, OnInit } from '@angular/core';
import { ProductDataService } from 'src/app/product-data.service';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.css']
})
export class ProductModalComponent implements OnInit {

  productThumbnailArray:any=["assets/images/heater-thumbnail-1.svg",
  "assets/images/thumbnail-2.svg",
  "assets/images/thumbnail-3.svg"];
  imageUrl:any="assets/images/heater-thumbnail-1.svg";

  constructor(public productOverview:ProductDataService) { 
    if(this.productOverview.dataArray.length===1){
      this.productOverview.isPrevButtonDisabled=true;
      this.productOverview.isNextButtonDisabled=true;
    }
  }

  ngOnInit(): void {
  }
  closeDetails(){
    this.productOverview.overview_toggle=false;
  }
  previousProduct(){
    if(this.productOverview.itemIndex>0){
      this.productOverview.itemIndex=this.productOverview.itemIndex-1;
    }
    this.productOverview.selectedProduct=this.productOverview.dataArray[this.productOverview.itemIndex];
    this.productOverview.isNextButtonDisabled=false;
    if(this.productOverview.itemIndex===0){
      this.productOverview.isPrevButtonDisabled=true;     
    }

  }
  nextProduct(){
    if(this.productOverview.itemIndex<this.productOverview.dataArray.length-1){
    this.productOverview.itemIndex=this.productOverview.itemIndex+1;
    }
    console.log(this.productOverview.dataArray[this.productOverview.itemIndex-1]);
    this.productOverview.selectedProduct=this.productOverview.dataArray[this.productOverview.itemIndex];
    this.productOverview.isPrevButtonDisabled=false;

    if(this.productOverview.itemIndex==this.productOverview.dataArray.length-1){
     this.productOverview.isNextButtonDisabled=true;
    }

  }

  changeImage(url:any){
    // console.log(this);
    this.imageUrl=url;
  }

}
