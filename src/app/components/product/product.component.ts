import { Component, OnInit } from '@angular/core';
import { ProductDataService } from 'src/app/product-data.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  data: any;


  constructor(public _product_data: ProductDataService) {
    this._product_data.getProductData()
      .subscribe(response => {
        this.data = response;
        this._product_data.dataArray = this.data.data.quotes.product_quotes;
        this._product_data.filteredDataArray = this._product_data.dataArray.filter((obj: any) => obj.volume === 50);

        // console.log(this._product_data.dataArray[0].volume);
        console.log(this._product_data.filteredDataArray);

      });
  }
  ngOnInit(): void {
  }




}
