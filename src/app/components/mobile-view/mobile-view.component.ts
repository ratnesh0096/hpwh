import { Component, OnInit } from '@angular/core';
import { ProductDataService } from 'src/app/product-data.service';

@Component({
  selector: 'app-mobile-view',
  templateUrl: './mobile-view.component.html',
  styleUrls: ['./mobile-view.component.css']
})
export class MobileViewComponent implements OnInit {

  constructor(public productOverview:ProductDataService) { }

  ngOnInit(): void {
  }

  showDetails(index:any, data:any){
    console.log("hello");
    // console.log("hello",this.product_info);
    // console.log("hello",this.productIndex);
    if(index==0){
      this.productOverview.isPrevButtonDisabled=true;
    }
    else{
      this.productOverview.isPrevButtonDisabled=false;
    }
    if(index==this.productOverview.dataArray.length-1){
      this.productOverview.isNextButtonDisabled=true;
    }
    else{
      this.productOverview.isNextButtonDisabled=false;
    }
    this.productOverview.overview_toggle=true
    this.productOverview.selectedProduct=data;
    this.productOverview.itemIndex=index;
  }


}
