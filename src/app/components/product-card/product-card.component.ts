import { Component, OnInit, Input} from '@angular/core';
import { ProductDataService } from 'src/app/product-data.service';
@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  @Input() product_info:any;
  @Input() productIndex:any;
  constructor(public productOverview:ProductDataService) {
    // @ViewChild("modal") modal:ElementRef;
    console.log(this.product_info);
   }
   ngOnInit(): void {}
   
  showDetails(index:any, data:any){
    console.log("hello");
    // console.log("hello",this.product_info);
    // console.log("hello",this.productIndex);
    if(index==0){
      this.productOverview.isPrevButtonDisabled=true;
    }
    else{
      this.productOverview.isPrevButtonDisabled=false;
    }
    if(index==this.productOverview.dataArray.length-1){
      this.productOverview.isNextButtonDisabled=true;
    }
    else{
      this.productOverview.isNextButtonDisabled=false;
    }
    this.productOverview.overview_toggle=true
    this.productOverview.selectedProduct=data;
    this.productOverview.itemIndex=index;
  }

  

}
